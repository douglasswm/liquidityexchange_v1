(function () {
    angular
        .module("RegApp", [])
        .controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http", '$window'];

    function RegCtrl($http,$window) {   // controller function passing the https service
        var vm = this;
        vm.username = ""; // form fields
        vm.email = "";
        vm.gender = "";
        vm.password = "";
        vm.passwordConfirm = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.match = false;
        vm.register = function () { // function to register
            $http.post("/register", { // post form info
                params: {
                    username: vm.username,
                    email: vm.email,
                    gender: vm.gender,
                    password: vm.password
                }
            }).then(function () {  // success
                console.info("success");
                vm.status.message = "Your registration is complete!";
                vm.status.code = 202;
                //redirect from here
                var url = "registration_2.html";
                $window.location.href = url;
            }).catch(function () {
                console.info("Error");  // error
                vm.status.message = "Your registration is failed!";
                vm.status.code = 400;
            });
        };
        
        vm.checkPassword = function(){
            if( vm.password === vm.passwordConfirm) {
                vm.match = true;
                console.log('pass match: ' + vm.match);
            }
            else {
                vm.match = false;
                console.log('fail match: ' + vm.match);
            }
        };
    }
})();


