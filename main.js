var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'));

app.post("/register", function(req, res, next) {
    var username = req.query.username;
    var email = req.query.email;
    var gender = req.query.gender;
    console.log("form data received");
    res.status(200).end()
});

//Set our port
app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

//Start server on port
app.listen(app.get("port"), function()  {
    console.info("Application started on port %d", app.get("port"))
});
